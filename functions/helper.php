<?php

# Help functions for the theme

// Format Phone Numbers Function
function format_phone($phone)
{
	$phone = preg_replace("/[^0-9]/", "", $phone);
 
	if(strlen($phone) == 7)
		return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
	elseif(strlen($phone) == 10)
		return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "$1-$2-$3", $phone);
	else
		return $phone;
}

// Format Phone Numbers Function
function format_phone_dot($phone)
{
	$phone = preg_replace("/[^0-9]/", "", $phone);
 
	if(strlen($phone) == 7)
		return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
	elseif(strlen($phone) == 10)
		return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "$1.$2.$3", $phone);
	else
		return $phone;
}

function short_string($string, $limit){
	$chars = preg_replace( "/\r|\n/", " ", $string );
	$chars = explode(' ',strip_tags($chars));
	$chars = array_slice($chars, 0, $limit);

	return join(" ", $chars);
}


// Get first image of a post
function catch_that_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches [1] [0];

  if(empty($first_img)){ //Defines a default image
    $first_img = "0";
  }
  return $first_img;
}


add_filter( 'embed_oembed_html', 'wpse_embed_oembed_html', 99, 4 );
function wpse_embed_oembed_html( $cache, $url, $attr, $post_ID ) {
    $classes = array();

    // Add these classes to all embeds.
    $classes_all = array(
        'responsive-container',
    );

    // Check for different providers and add appropriate classes.

    if ( false !== strpos( $url, 'vimeo.com' ) ) {
        $classes[] = 'vimeo';
    }

    if ( false !== strpos( $url, 'youtube.com' ) ) {
        $classes[] = 'youtube';
    }

    $classes = array_merge( $classes, $classes_all );

    return '<div class="' . esc_attr( implode( $classes, ' ' ) ) . '">' . $cache . '</div>';
}


add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {
    $existing_mimes['dwg'] = 'application/dwg';
    return $existing_mimes;
}




?>