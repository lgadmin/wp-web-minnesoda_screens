<?php

function ajax_enqueue() {
      wp_localize_script( 'lg-script', 'lg_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
 }
 add_action( 'wp_enqueue_scripts', 'ajax_enqueue' );


add_action( 'wp_ajax_gallery_modal', 'gallery_modal' );
add_action( 'wp_ajax_nopriv_gallery_modal', 'gallery_modal' );

function gallery_modal(){

	$gallery_query = array(
		'showposts' => -1,
		'post_type' => 'gallery',
		'p'         => $_POST['gallery_id']
	);

	$gallery_query_results = new WP_Query( $gallery_query );  
	$gallery_items = [];

	if($gallery_query_results->have_posts()){
		while ( $gallery_query_results->have_posts() ){
			$gallery_query_results->the_post();

			if( have_rows('gallery_items') ){
				while ( have_rows('gallery_items') ){
					the_row();
					$gallery_item = new stdClass();
					$gallery_item->copy = get_sub_field('ms_gallery_image_copy');
					$gallery_item->image = get_sub_field('ms_gallery_image');

					array_push($gallery_items, $gallery_item);
				}
			}

		}
	}		        

	wp_reset_query();

	wp_send_json($gallery_items);

}

?>