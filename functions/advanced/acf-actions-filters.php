<?php

function lg_acf_remove_hyphen( $string ) {
    return str_replace("---","",$string);
}

function acf_load_type_choices( $field ) {
    global $post;

    $types = get_field('product_type');
    //var_dump($types);
    // reset choices
    $field['choices'] = array();
    $choices = array();

    if(is_array($types) || is_object($types)){
        foreach ($types as $key => $type) {
            array_push($choices, lg_acf_remove_hyphen($type['title']));
        }
    }

    if( is_array($choices) ) {
        foreach( $choices as $choice ) {
            $field['choices'][ $choice ] = $choice;
        }
    }
    return $field;
}

function acf_load_size_choices( $field ) {
    global $post;

    $types = get_field('product_sizes');
    // reset choices
    $field['choices'] = array();
    $choices = array();

    if(is_array($types) || is_object($types)){
        foreach ($types as $key => $type) {
            array_push($choices, lg_acf_remove_hyphen($type['type']) . ' --- ' . lg_acf_remove_hyphen($type['width']) . 'X' . lg_acf_remove_hyphen($type['height']));
        }
    }

    if( is_array($choices) ) {
        foreach( $choices as $choice ) {
            $field['choices'][ $choice ] = $choice;
        }
    }
    return $field;
}

add_filter('acf/load_field/key=field_5ab40f6d83f0d', 'acf_load_type_choices');
add_filter('acf/load_field/key=field_5ab420f138e20', 'acf_load_size_choices');

?>