<?php

function lg_enqueue_styles_scripts() {
    wp_enqueue_style( 'lg-style', get_stylesheet_directory_uri() . '/style.css', [], wp_get_theme()->get('Version') );
	
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Lato:300,400,700|Roboto:300,400,700', false );
	wp_enqueue_style( 'fontawesome5', 'https://use.fontawesome.com/releases/v5.0.8/css/all.css', false );

	wp_register_script( 'lg-script', get_stylesheet_directory_uri() . "/assets/dist/js/script.min.js", array('jquery'), '1.0.1' );
	wp_register_script( 'vendorJS', get_stylesheet_directory_uri() . "/assets/dist/js/vendor.min.js", array('jquery'), false );

	wp_enqueue_script( 'lg-script' );
	wp_enqueue_script( 'vendorJS' );
}
add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles_scripts' );


//Dequeue JavaScripts
function lg_dequeue_scripts() {
	wp_dequeue_script( '_s-navigation' );
	wp_deregister_script( '_s-navigation' );
}
//add_action( 'wp_print_scripts', 'lg_dequeue_scripts' );

?>