<?php

function create_taxonomy(){

	register_taxonomy(
		'product-category',
		'product',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( 'slug' => 'product-category' ),
			'hierarchical' => true,
		)
	);

	register_taxonomy(
		'product-colour-category',
		'product-colour',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( 'slug' => 'product-colour-category' ),
			'hierarchical' => true,
		)
	);
}

add_action( 'init', 'create_taxonomy' );

?>