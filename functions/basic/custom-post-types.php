<?php

//Custom Post Types
function create_post_type() {

    // PRODUCT
    register_post_type( 'product',
        array(
          'labels' => array(
            'name' => __( 'Products' ),
            'singular_name' => __( 'Product' )
          ),
          'public' => true,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'minnesota_screens',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    // PRODUCT
    register_post_type( 'product-colour',
        array(
          'labels' => array(
            'name' => __( 'Product Colours' ),
            'singular_name' => __( 'Product Colour' )
          ),
          'public' => true,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'minnesota_screens',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    // Gallery
    register_post_type( 'Gallery',
        array(
          'labels' => array(
            'name' => __( 'Galleries' ),
            'singular_name' => __( 'Gallery' )
          ),
          'public' => true,
          'has_archive' => false,
          // 'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'minnesota_screens',   #### Main menu slug
          'supports' => array( 'thumbnail','title' )
        )
    );

    // Testimonials
    register_post_type( 'Testimonials',
        array(
          'labels' => array(
            'name' => __( 'Testimonials' ),
            'singular_name' => __( 'Testimonials' )
          ),
          'public' => true,
          'has_archive' => true,
          // 'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'minnesota_screens',   #### Main menu slug
          'supports' => array( 'title', 'editor' )
        )
    );

    // Events
    register_post_type( 'Events',
        array(
          'labels' => array(
            'name' => __( 'Events' ),
            'singular_name' => __( 'Event' )
          ),
          'public' => true,
          'has_archive' => false,
          // 'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'minnesota_screens',   #### Main menu slug
          'supports' => array( 'title', 'editor' )
        )
    );


}
add_action( 'init', 'create_post_type' );

?>