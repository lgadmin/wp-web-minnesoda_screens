// Windows Load Handler

(function($) {

    $(window).on('load', function(){

    	sticky_header_initialize();
    	KCollection.RepositionFeatureItem([],'.home .feature-banner', 1600);

    });

}(jQuery));