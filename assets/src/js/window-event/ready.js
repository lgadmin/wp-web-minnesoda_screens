// Windows Ready Handler

(function($) {

    $(document).ready(function(){

      	//Mobile Toggle
        $('.mobile-toggle').on('click', function(){
      		$('.navbar-collapse').fadeToggle();
      	});

        $('.dropdown-toggle').on('click', function(e){
          e.preventDefault();
        });

        $('.dropdown-toggle').closest('li').on('mouseover', function(e){
          $(this).addClass('open');
        });

        $('.dropdown-toggle').closest('li').on('mouseleave', function(e){
          $(this).removeClass('open');
        });

        /*var scrollTo = $(this).attr('href');
        var settings = {
            duration: 2 * 1000,
            offset: ($('.site-header').outerHeight()) * -1
        };

        KCollection.headerScrollTo(scrollTo, settings);*/


      //If Mega Menu
      /*$('.mobile-toggle').on('click', function(){
        $('.mega-menu-toggle').click();
      });*/

      $('.quick-nav .thumbnail').on('click', function(e){
        e.preventDefault();
        var target = $(this).attr('target');
        var gallery_id = $(this).attr('gallery_id');

        var data = {
          'action': 'gallery_modal',
          'gallery_id': gallery_id
        };

        $.post(lg_ajax.ajax_url, data, function(response) {
          var modal_body = $(target).find('.modal-body');
          modal_body.empty();
          modal_body.append('<div class="modal-gallery">');


          for( var i=0; i<response.length; i++){
            modal_body.find('.modal-gallery').append('<div class="slide-container">' +
              '<div class="slide-cta">' + response[i].copy +
              '</div>' +
              '<div class="img-cont"><img src="' + response[i].image['url'] + '">' +
              '</div>' +
              '</div>'
              );
          }

          modal_body.append('</div>');

          $(target).fadeIn().addClass('in').find('.modal-gallery').slick({
            lazyLoad       : 'ondemand',
            autoplay       : true,
            infinite       : true,
            slidesToShow   : 1,
            slidesToScroll : 1,
            arrows         : true,
            // variableWidth  : true
          });
        });
      });

      $('.close').on('click', function(){
        $('.modal.gallery').fadeOut().removeClass('in');
      });



  jQuery('.scroll-down').click(function(event) {
    event.preventDefault();
    var n = jQuery(document).height() / 6;
    jQuery('html, body').animate({ scrollTop: n }, 1000);
  });


      
        
    });

}(jQuery));