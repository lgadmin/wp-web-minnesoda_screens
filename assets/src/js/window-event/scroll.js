// Window Scroll Handler

(function($) {

    $(window).on('scroll', function(){
     
     	var utility_height = $('.utility-bar').outerHeight();

		if ($(this).scrollTop() > 100 && !$('#masthead').hasClass('scroll-state')) {
			 $('#masthead').addClass('scroll-state');
			 $('.utility-bar').stop().animate({ marginTop: (-1) * utility_height }, 100, 'linear');
		}
		if ($(this).scrollTop() < 100 && $('#masthead').hasClass('scroll-state')) {
			 $('#masthead').removeClass('scroll-state');
			 $('.utility-bar').stop().animate({ marginTop: 0 }, 100, 'linear');
		}

    });

}(jQuery));