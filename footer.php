<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	<?php get_template_part( '/templates/template-parts/footer/cta-footer' ); ?>

	<div class="pt-sm pb-sm vendor-logos">
		<div class="container">
			<?php echo do_shortcode('[lg-slider id=32]'); ?>
		</div>
	</div>
	
	<footer id="colophon" class="site-footer">
		
		<!-- Primary Footer -->
		<div id="site-footer" class="bg-primary">
		
			<div class="container pt-sm pb-sm">
				
				<div class="site-footer-alpha">
					<h2 class="footer-brand-heading"><a href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo get_bloginfo( 'name' ); ?></a></h2>
					<div class="small"><?php echo get_field('footer_blurb', 'option'); ?></div>
				</div>

				<div class="site-footer-bravo">
					<h2 class="nav-title">PRODUCTS</h2>
					<?php
						wp_nav_menu( array(
							'theme_location'        => 'bottom-nav-products',
							'depth'          => 1,
							'container'      => 'nav'
						) );
					?>
				</div>

				<div class="site-footer-charlie">
					<h2 class="nav-title">ABOUT US</h2>
					<?php
						wp_nav_menu( array(
							'theme_location'        => 'bottom-nav-00',
							'depth'          => 1,
							'container'      => 'nav'
						) );
					?>
				</div>

				<div class="site-footer-delta">
					<h2 class="nav-title">CONTACT US</h2>
					<?php get_template_part("/templates/template-parts/footer/address-card"); ?>
				</div>

				<div class="site-footer-echo">
					<h2 class="nav-title">STAY CONNECTED</h2>
					<?php echo do_shortcode('[lg-social-media list="instagram, rss, youtube, facebook, twitter, linkedin"]'); ?>
				</div>

			</div>
		</div>

		<!-- Legal -->
		<div id="site-legal" class="bg-gray-light pb-xs pt-xs pl-md pr-md">
			<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
			<div class="site-longevity"> <?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
		</div>

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
