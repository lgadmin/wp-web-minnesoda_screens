<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */
 
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

	<header id="masthead" class="site-header sticky-header">
    <div class="header-content">

    <?php get_template_part("/templates/template-parts/header/site-utility-bar"); ?>

    <?php get_template_part("/templates/template-parts/header/nav-desktop"); ?>

    <?php get_template_part("/templates/template-parts/header/nav-mobile"); ?>

    </div>

  </header><!-- #masthead -->


  <?php 
    if (is_front_page()) {
        get_template_part("/templates/template-parts/header/feature-banner", 'home'); 
    } elseif(is_home()) {
        get_template_part("/templates/template-parts/header/feature-banner", 'blog'); 
    } elseif(is_post_type_archive( 'testimonials' )) {
        get_template_part("/templates/template-parts/header/feature-banner", 'testimonials'); 
    } elseif(is_tax()){
        get_template_part("/templates/template-parts/header/feature-banner", 'tax'); 
    } elseif(is_singular('product')){
        get_template_part("/templates/template-parts/header/feature-banner", 'home');
    }else{
        get_template_part("/templates/template-parts/header/feature-banner"); 
    } 

  ?>

