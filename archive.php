<?php get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			
			<main>
				
				<header class="page-title mt-sm mb-md"><h1 class="h3"><?php single_post_title(); ?></h1></header>
				
				<div class="post-container">
					<div class="body-copy">
						<?php get_template_part( 'templates/template-parts/content', 'blog') ?>
					</div>
					<?php get_sidebar(); ?>
				</div>

			</main>
		</div>
	</div>

<?php get_footer(); ?>