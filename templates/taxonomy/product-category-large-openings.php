<?php

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content single-product">

			<main>
	
			<?php
				$term = get_queried_object();
				?>
				<div class="large-opening-archive block pt-lg pb-lg">
					<div class="container">
						<header class="page-title">
							<h1 class="h3"><?php single_term_title(); ?></h1>

						<?php
							$args = array(
					            'showposts'	=> -1,
					            'post_type'		=> 'product',
					            'tax_query' => array(
									array(
										'taxonomy' => 'product-category',
										'field'    => 'ID',
										'terms'    => array($term->term_id)
									),
								),
					        );
					        $result = new WP_Query( $args );

					        // Loop
					        if ( $result->have_posts() ) :
					        	?>
					        	<div class="cta-quick-nav product-taxonomy container">
					        		<div class="cta-body pt-sm">
					        			<section class="quick-nav">
								        	<?php
								            while( $result->have_posts() ) : $result->the_post();
								        	?>
								        		<div class="quick-nav-item">
								        			<a href="<?php echo get_permalink(); ?>" class="thumbnail-quick">
								        				<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
								        				<div class="caption">
								        					<?php the_title(); ?>
								        				</div>
								        			</a>
								        		</div>
											<?php
								            endwhile;
								            ?>
						            	</section>
					            	</div>
					            </div>
					            <?php
					        endif; // End Loop

					        wp_reset_query();
						?>
					</div>
				</div>

			</main>

		</div>
	</div>

<?php get_footer(); ?>