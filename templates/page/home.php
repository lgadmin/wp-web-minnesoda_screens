<?php

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>
				

				<!-- Quote CTA -->
				<?php// get_template_part("/templates/template-parts/get-a-quote-cta"); ?>
				<!-- end Quote CTA -->

			</main>

			<?php get_template_part( '/templates/template-parts/cta-flexible/cta-flexible' ); ?>

		</div>
	</div>

<?php get_footer(); ?>