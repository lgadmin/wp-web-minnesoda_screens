<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">

			<div class="pt-lg pb-lg container">
				<?php echo do_shortcode('[gravityform id=1 title=true description=true ajax=true tabindex=49]'); ?>
			</div>
			
			<div class="bg-pattern-alpha clearfix">
				<main class="container">
					<?php get_template_part( 'templates/template-parts/content' ) ?>
				</main>
			</div>

			<?php get_template_part( '/templates/template-parts/cta-flexible/cta-flexible' ); ?>

			<section class="container">
				<?php get_template_part( '/templates/template-parts/events/events-content' ); ?>
			</section>

		</div>
	</div>

<?php get_footer(); ?>