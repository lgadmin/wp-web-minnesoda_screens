<?php

function product_size_format( $string ){
	$chars = explode("X",$string);

	return join(" X ", $chars);
}

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content single-product">

			<main>
	
			<!-- Intro Section -->
			<?php
				$intro_content = get_field('intro_content');
				$intro_media = get_field('intro_media');
			?>
			<div class="bg-pattern-alpha pt-lg pb-lg intro-section">
				<div class="cta-alpha container">
					<header class="page-title">
						<h1 class="h3"><?php the_title(); ?></h1>
						<?php the_field('h1_context'); ?>
					</header>
						<div class="cta-body pt-md split-content">
							<div class="split-image">
								<?php if($intro_media['media_type'] == 'Video'): ?>
								<?php echo $intro_media['video']; ?>
								<?php elseif($intro_media['media_type'] == 'Image'): ?>
									<img src="<?php echo $intro_media['image']['url']; ?>" alt="<?php echo $intro_media['image']['alt']; ?>">
								<?php endif; ?>
							</div>
							<div class="split-copy">
								<?php echo $intro_content; ?>
							</div>
						</div>
					</div>
			</div>
			<!-- end Intro Section -->

			<!-- Tab Section -->
			<?php
				$type_activate = get_field('type_activate');
				$size_activate = get_field('size_activate');
				$colour_activate = get_field('colour_activate');
				$mesh_activate = get_field('mesh_activate');
				$pricing_activate = get_field('pricing_activate');

				$mesh_content = get_field('mesh_content');
				$product_sizing_content = get_field('product_sizing_content');
				$color_option_content = get_field('color_option_content');
			?>

			<section class="container mt-md mb-sm product-tabs product-tab">

				<ul class="nav nav-pills nav-justified" role="tablist">
					<?php if($type_activate): ?>
					<li role="presentation" class="active"><a href="#types" aria-controls="types" role="tab" data-toggle="tab">types</a></li>
					<?php endif; ?>
					<?php if($size_activate): ?>
					<li role="presentation"><a href="#sizes" aria-controls="sizes" role="tab" data-toggle="tab">sizes</a></li>
					<?php endif; ?>
					<?php if($colour_activate): ?>
					<li role="presentation"><a href="#colors" aria-controls="colors" role="tab" data-toggle="tab">colors</a></li>
					<?php endif; ?>
					<?php if($mesh_activate): ?>
					<li role="presentation"><a href="#mesh" aria-controls="mesh" role="tab" data-toggle="tab">mesh</a></li>
					<?php endif; ?>
					<?php if($pricing_activate): ?>
					<li role="presentation"><a href="#pricing" aria-controls="pricing" role="tab" data-toggle="tab">pricing</a></li>
					<?php endif; ?>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<?php if($type_activate): ?>
					<div role="tabpanel" class="tab-pane active" id="types">
						<?php
						if( have_rows('product_type') ):
							?>
							<div class="product-types">
							<?php
						    while ( have_rows('product_type') ) : the_row();
						        $title = get_sub_field('title');
						        $image = get_sub_field('image');
						        $content = get_sub_field('content');
						        ?>
								<div class="single-product-type">
									<div class="split-image">
										<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
									</div>
									<div class="split-copy">
										<?php if($title): ?>
										<h3><?php echo $title; ?></h3>
										<?php endif; ?>
										<?php echo $content; ?>
									</div>
								</div>
						        <?php
						    endwhile;
						    ?>
						    </div>
						    <?php
						else :
						    // no rows found
						endif;
						?>
					</div>
					<?php endif; ?>

					<?php if($size_activate): ?>
					<div role="tabpanel" class="tab-pane" id="sizes">
						<?php echo $product_sizing_content; ?>
						<?php
						if( have_rows('product_sizes') ):
							?>
							<div class="product-size-wrap">
							<table class="product-size">
								<tr>
									<th>Type/Style</th>
									<th>Width</th>
									<th>Height</th>
								</tr>
							<?php
						    while ( have_rows('product_sizes') ) : the_row();
						        $type = get_sub_field('type');
						        $width = get_sub_field('width');
						        $height = get_sub_field('height');
						        ?>
								<tr class="single-product-size">
									<td><?php echo $type; ?></td>
									<td><?php echo $width; ?></td>
									<td><?php echo $height; ?></td>
								</tr>
						        <?php
						    endwhile;
						    ?>
						    </table></div>
						    <?php
						else :
						    // no rows found
						endif;
						?>
					</div>
					<?php endif; ?>

					<?php if($colour_activate): ?>
					<div role="tabpanel" class="tab-pane" id="colors">
						<?php echo $color_option_content; ?>
						<hr>
						<div class="content">
							<?php 
							$posts = get_field('signature_colours');
							if( $posts ): ?>
							<h3>Signature Color Options</h3>
							    <ul class="color-list">
							    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
							        <?php setup_postdata($post); ?>
							        <li>
							            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
							            <div><?php the_title(); ?></div>
							        </li>
							    <?php endforeach; ?>
							    </ul>
							    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
							<?php endif; ?>
						</div>
						<hr>
						<div class="content">
							<?php 
							$posts = get_field('custom_colours');
							if( $posts ): ?>
							<h3>Custom Color Options</h3>
							    <ul class="color-list-custom">
							    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
							        <?php setup_postdata($post); ?>
							        <li>
							            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
							            <div><?php the_title(); ?></div>
							        </li>
							    <?php endforeach; ?>
							    </ul>
							    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
							<?php endif; ?>
						</div>
					</div>
					<?php endif; ?>

					<?php if($mesh_activate): ?>
					<div role="tabpanel" class="tab-pane" id="mesh">
						<?php echo $mesh_content; ?>
						<?php
						if( have_rows('mesh_types') ):
							?>
							<div class="product-mesh-wrap">
							<table class="product-mesh">
								<tr>
									<th>Type</th>
									<th>Style</th>
									<th>Openness</th>
									<th>Colour</th>
									<th>Protection</th>
									<th>Sample</th>
								</tr>
							<?php
						    while ( have_rows('mesh_types') ) : the_row();
						        $type = get_sub_field('type');
						        $style = get_sub_field('style');
						        $openness = get_sub_field('openness');
						        $sunuv = get_sub_field('sunuv');
						        $protection = get_sub_field('protection');
						        $sample = get_sub_field('sample');
						        ?>
								<tr class="single-product-mesh">
									<td><?php echo $type; ?></td>
									<td><?php echo $style; ?></td>
									<td><?php echo $openness; ?></td>
									<td><?php echo $sunuv; ?></td>
									<td><?php echo $protection; ?></td>
									<td><img src="<?php echo $sample['url']; ?>" alt="<?php echo $sample['alt']; ?>"></td>
								</tr>
						        <?php
						    endwhile;
						    ?>
						    </table></div>
						    <?php
						else :
						    // no rows found
						endif;
						?>
					</div>
					<?php endif; ?>
					<?php if($pricing_activate): ?>
					<div role="tabpanel" class="tab-pane" id="pricing">
						<?php
						if( have_rows('price_list') ):
							?>
							<div class="product-price">
							<?php
						    while ( have_rows('price_list') ) : the_row();
						        $image = get_sub_field('image');
						        $price = get_sub_field('price');
						        $option = explode(' --- ', get_sub_field('type_+_size'));
						        $type = $option[0];
						        $size = product_size_format($option[1]);
						        ?>
								<div>
									<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
									<div class="price-content">
										<div class="type"><?php echo $type; ?></div>
										<div class="size"><?php echo $size; ?></div>
										<div class="price"><?php echo $price; ?></div>
									</div>
								</div>
						        <?php
						    endwhile;
						    ?>
						    </div>
						    <?php
						else :
						    // no rows found
						endif;
						?>

						<?php
						if( have_rows('price_terms_and_condictions') ):
							?>
							<ul class="terms_conditions">
							<?php
						    while ( have_rows('price_terms_and_condictions') ) : the_row();
						        $term = get_sub_field('term');
						        ?>
								<li><?php echo $term; ?></li>
						        <?php
						    endwhile;
						    ?>
						    </ul>
						    <?php
						else :
						    // no rows found
						endif;
						?>
					</div>
					<?php endif; ?>
				</div>

			</section>
			<!-- end Tab Section -->

			</main>

			<?php get_template_part( '/templates/template-parts/cta-flexible/cta-flexible' ); ?>

		</div>
	</div>

<?php get_footer(); ?>