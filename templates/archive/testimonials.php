<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			
			<main>
				
				<header class="page-title"><h1 class="h3">Testimonials</h1></header>
				
				<div class="post-container">
					<div class="body-copy">
						<?php get_template_part( 'templates/template-parts/content', 'testimonials') ?>
						<?php the_posts_navigation(); ?>
					</div>
				</div>

			</main>
		</div>
	</div>

<?php get_footer(); ?>