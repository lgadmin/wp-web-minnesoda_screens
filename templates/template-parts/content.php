	<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class('type'); ?>>
				<header class="page-title">
					<h1 class="h3"><?php the_title(); ?></h1>
					<?php the_field('h1_context'); ?>
				</header>
				<div class="entry-content">
					<?php the_content(); ?>
				</div>
			</article>

		<?php endwhile; ?>
	<?php endif ?>