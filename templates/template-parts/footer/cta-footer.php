<section class="clearfix pt-md pb-md">
	<div class="cta-alpha container">
		<?php if (get_field('cta_title', 'option')) : ?>
			<header class="page-title"><span class="h3"><?php the_field('cta_title_content', 'option'); ?></span></header>
		<?php endif; ?>
		
		<div class="cta-body pt-sm">
			<?php the_field('alpha_cw', 'option'); ?>
			<div class="text-center mt-sm">
				<a href="<?php the_field('footer_cta_button_link', 'option'); ?>" class="btn btn-primary btn-lg"><?php the_field('footer_cta_button', 'option'); ?></a>
			</div>
		</div>
	</div>
</section>
