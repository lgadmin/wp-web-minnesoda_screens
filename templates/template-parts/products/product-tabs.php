<section class="container mt-md mb-sm product-tabs">

  <!-- Nav tabs -->
  <ul class="nav nav-pills nav-justified" role="tablist">
    <li role="presentation" class="active"><a href="#types" aria-controls="types" role="tab" data-toggle="tab">Types</a></li>
    <li role="presentation"><a href="#sizes" aria-controls="sizes" role="tab" data-toggle="tab">Sizes</a></li>
    <li role="presentation"><a href="#colors" aria-controls="colors" role="tab" data-toggle="tab">Colors</a></li>
    <li role="presentation"><a href="#mesh" aria-controls="mesh" role="tab" data-toggle="tab">Mesh</a></li>
    <li role="presentation"><a href="#pricing" aria-controls="pricing" role="tab" data-toggle="tab">Pricing</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="types">
    	<h2>Types</h2>
    	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis esse dolore, libero id doloremque necessitatibus recusandae, obcaecati cum quisquam magni mollitia asperiores. Beatae error, laborum! Totam, est? Cum, provident, voluptatem.</p>
    </div>
    <div role="tabpanel" class="tab-pane" id="sizes">
    	<h2>Sizes</h2>
    	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis esse dolore, libero id doloremque necessitatibus recusandae, obcaecati cum quisquam magni mollitia asperiores. Beatae error, laborum! Totam, est? Cum, provident, voluptatem.</p>
    </div>
    <div role="tabpanel" class="tab-pane" id="colors">
    	<h2>Colors</h2>
    	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis esse dolore, libero id doloremque necessitatibus recusandae, obcaecati cum quisquam magni mollitia asperiores. Beatae error, laborum! Totam, est? Cum, provident, voluptatem.</p>
    </div>
    <div role="tabpanel" class="tab-pane" id="mesh">
     	<h2>Mesh</h2>
    	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis esse dolore, libero id doloremque necessitatibus recusandae, obcaecati cum quisquam magni mollitia asperiores. Beatae error, laborum! Totam, est? Cum, provident, voluptatem.</p>   	
    </div>
    <div role="tabpanel" class="tab-pane" id="pricing">
     	<h2>Pricing</h2>
    	<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis esse dolore, libero id doloremque necessitatibus recusandae, obcaecati cum quisquam magni mollitia asperiores. Beatae error, laborum! Totam, est? Cum, provident, voluptatem.</p>   	
    </div>
  </div>

</section> 

