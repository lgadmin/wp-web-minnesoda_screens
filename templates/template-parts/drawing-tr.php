<tr>
	<td><?php the_sub_field('drawing_and_downloads_title'); ?></td>
	<td><?php the_sub_field('drawing_and_downloads_description'); ?></td>
	<td class="text-center">
		<?php if (get_sub_field('drawing_and_downloads_autocad')) : ?>
			<a href="<?php the_sub_field('drawing_and_downloads_autocad'); ?>" download><i class="fas fa-file"></i></a>
		<?php endif; ?>
	</td>
	<td class="text-center">
		<?php if (get_sub_field('drawing_and_downloads_pdf')) : ?>
			<?php the_sub_field('drawing_and_downloads_pdf'); ?>
		<?php endif; ?>
	</td>
</tr>