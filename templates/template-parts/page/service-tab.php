


<?php if( have_rows('service_tab') ): ?>

  <section class="container mt-md mb-sm product-tabs">

    <?php if (get_field('title_heading')) : ?>
      <header class="page-title"><span class="h3"><?php the_field('title_heading'); ?></span></header>
    <?php endif; ?>
    
    <div class="cta-body mb-md">
      <?php the_field('title_description'); ?>
    </div>


    <!-- Nav tabs -->
    <ul class="nav nav-pills nav-justified" role="tablist">
      <?php $i = 0; ?>
      <?php while( have_rows('service_tab') ): the_row(); ?>
        <?php 
          $tabID = get_sub_field('service_tab_label');
          $tabID =  str_replace(" ", "-", $tabID); 
          $tabID = strtolower($tabID);
        ?>
        <li role="presentation" <?php if ($i == 0) { echo 'class="active"'; } ?> ><a href="#<?php echo $tabID; ?>" aria-controls="<?php echo $tabID; ?>" role="tab" data-toggle="tab"><?php the_sub_field('service_tab_label'); ?></a></li>
        <?php $i++; ?>
      <?php endwhile; ?>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
      <?php $ii = 0; ?>
      <?php while( have_rows('service_tab') ): the_row(); ?>    
        
        <?php 
          $tabID = get_sub_field('service_tab_label');
          $tabID = str_replace(" ", "-", $tabID);
          $tabID = strtolower($tabID);
        ?>
        
        <div role="tabpanel" class="tab-pane <?php if ($ii == 0) { echo 'active'; } ?>" id="<?php echo $tabID; ?>"> <?php the_sub_field('service_tab_content'); ?> </div>
      <?php $ii++; ?>
      <?php endwhile; ?>  
    </div>

  </section> 

<?php endif; ?>
