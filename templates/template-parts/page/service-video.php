<section class="clearfix pt-md">
	<div class="cta-alpha container">
		<div class="cta-body pt-sm">
			<div class="myvideo">
				<?php the_field('video_content'); ?>
			</div>
		</div>
	</div>
</section>
