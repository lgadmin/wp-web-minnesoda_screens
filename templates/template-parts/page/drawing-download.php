
<?php 
	// Create a table of Headings from ACF Select Field. Array named $mytable
    if( have_rows('drawing_and_downloads') ) {
       
        $mytable = array();
        
        while ( have_rows('drawing_and_downloads') ){
           
            the_row();
            
            if (! in_array(get_sub_field('drawing_and_downloads_section'), $mytable) ) {
                array_push( $mytable, get_sub_field('drawing_and_downloads_section'));
                // echo '<pre>'; print_r($mytable); echo '</pre>';
            }
            
        }
        
    }
?>


<?php foreach ($mytable as $iheading) : ?>
<section class="drawing-table container mt-sm">
    <h2><?php echo $iheading; ?></h2>
	<table class="events-table table-responsive">
		
		<thead>
			<tr class="bg-primary">
				<th class="dd-title"><span class="sr-only">Title</span></th>
				<th class="dd-description"><span class="sr-only">Description</span></th>
				<th class="dd-item">AutoCAD</th>
				<th class="dd-item">PDF</th>
			</tr>
		</thead>

		<tbody>

		    <?php while ( have_rows('drawing_and_downloads') ) : the_row(); ?>
		        <?php if ( get_sub_field('drawing_and_downloads_section') == $iheading ) : ?>

					<tr>
						<td><?php the_sub_field('drawing_and_downloads_title'); ?></td>
						<td><?php the_sub_field('drawing_and_downloads_description'); ?></td>
						<td class="text-center">
							<?php if (get_sub_field('drawing_and_downloads_autocad')) : ?>
								<?php $min_cad = get_sub_field('drawing_and_downloads_autocad'); ?>
								<a href="<?php the_sub_field('drawing_and_downloads_autocad'); ?>" download><i class="fas fa-file"></i></a>
							<?php endif; ?>
						</td>
						<td class="text-center">
							<?php if (get_sub_field('drawing_and_downloads_pdf')) : ?>

								<?php $min_pdf = get_sub_field('drawing_and_downloads_pdf'); ?>

								<a href="<?php echo $min_pdf['url']; ?>" download><i class="fas fa-file-pdf"></i></a>

							<?php endif; ?>
						</td>
					</tr>

		        <?php endif; ?>
		    <?php endwhile; ?>

		</tbody>

	</table>
</section>

<?php endforeach; ?>

