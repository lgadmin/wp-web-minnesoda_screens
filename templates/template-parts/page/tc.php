<section class="bg-pattern-alpha clearfix">
	<div class="cta-alpha container">		
		<div class="cta-body pt-sm">
			<div class="text-center">
				<button class="btn btn-primary mb-sm" role="button" data-toggle="collapse" href="#tc-collapse" aria-expanded="false" aria-controls="tc-collapse"> Terms &amp; Conditions &nbsp; <i class="fas fa-angle-right"></i> </button>
			</div>
			<div class="collapse" id="tc-collapse">
				<?php the_field('terms_&_conditions'); ?>
			</div>
		</div>
	</div>
</section> 