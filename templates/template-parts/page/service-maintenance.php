<?php

	$service_and_maintenance_image = get_field('service_and_maintenance_image','option');
	$service_and_maintenance_copy = get_field('service_and_maintenance_copy','option');
	$service_and_maintenance_button_text = get_field('service_and_maintenance_button_text','option');
	$service_and_maintenance_button_link = get_field('service_and_maintenance_button_link','option');

?>
<div class="part-service-maintenance container pt-lg pb-lg">
	<header class="page-title"><span class="h3">SERVICE AND MAINTENANCE</span></header>
	<div class="content split-content pt-md">
		<div class="split-image">
			<img src="<?php echo $service_and_maintenance_image['url']; ?>" alt="<?php echo $service_and_maintenance_image['alt']; ?>">
		</div>
		<div class="split-copy">
			<?php echo $service_and_maintenance_copy; ?>
			<a href="<?php echo $service_and_maintenance_button_link; ?>" class="btn btn-primary btn-lg"><?php echo $service_and_maintenance_button_text; ?></a>
		</div>
	</div>
</div>