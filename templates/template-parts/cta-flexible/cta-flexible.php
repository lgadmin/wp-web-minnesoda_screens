
<?php if( have_rows('cta') ):
	while ( have_rows('cta') ) : the_row();
		
		switch ( get_row_layout()) {

			// Alpha
			case 'cta_alpha':
				get_template_part('/templates/template-parts/cta-flexible/cta-alpha');
			break;

			// Quick Nav
			case 'cta_quick_nav':
				get_template_part('/templates/template-parts/cta-flexible/cta-quick-nav');
			break;
			
			// Quick Nav Alpha
			case 'cta_quick_nav_alpha':
				get_template_part('/templates/template-parts/cta-flexible/cta-quick-nav-alpha');
			break;

			// Quick Nav Bravo
			case 'cta_quick_nav_bravo':
				get_template_part('/templates/template-parts/cta-flexible/cta-quick-nav-bravo');
			break;

			// Gallery Feature Slider
			case 'cta_gallery_feature_slider':
				get_template_part('/templates/template-parts/cta-flexible/cta-gallery-feature-slider');
			break;

			// Gallery Modal
			case 'cta_gallery_modal':
				get_template_part('/templates/template-parts/cta-flexible/cta-gallery-modal');
			break;

			// Gallery Modal Youtube
			case 'cta_gallery_youtube':
				get_template_part('/templates/template-parts/cta-flexible/cta-gallery-modal-youtube');
			break;

			// CTA Half and Half
			case 'cta_half_and_half':
				get_template_part('/templates/template-parts/cta-flexible/cta-half-half');
			break;

			default:
				echo "<!-- nothing to see here -->";
			break;
		}

	endwhile; else : // no layouts found 
endif; ?>