<section class="<?php the_sub_field('background_colour'); ?> clearfix pt-sm pb-md cta-half-half">
	
	<div class="cta_half_and_half <?php the_sub_field('container'); ?>">
		<?php if (get_sub_field('cta_title')) : ?>
			<header class="page-title"><span class="h3"><?php the_sub_field('cta_title_content'); ?></span></header>
		<?php endif; ?>
		
		<div class="half-half-cont">
			<div class="cta-left-half">
				<?php the_sub_field('cta_left_half'); ?>
			</div>

			<div class="cta-right-half">
				<?php the_sub_field('cta_right_half'); ?>
			</div>
		</div>

	</div>
</section>
