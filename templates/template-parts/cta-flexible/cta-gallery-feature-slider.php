<section class="<?php the_sub_field('background_colour'); ?> clearfix pt-sm pb-md">


    <div class="cta-alpha <?php the_sub_field('container'); ?>">

        <?php if (get_sub_field('cta_title')) : ?>
          <header class="page-title"><span class="h3"><?php the_sub_field('cta_title_content'); ?></span></header>
        <?php endif; ?>

   
        <?php  $images = get_sub_field('cta_gallery_feature_gallery');

        if( $images ): ?>

            <!-- Slide -->
            <div class="slider-for">
                <?php foreach( $images as $image ): ?>
                    <div class="slick-container">
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                        
                    </div>
                <?php endforeach; ?>
            </div>

           <!-- Slide Navigation -->
           <div class="slider-nav">
                <?php foreach( $images as $image ): ?>
                    <div class="nav-img-cont"> <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /> </div>
                <?php endforeach; ?>
            </div>

        <?php endif; ?>

    </div>
</section>

<script>

    jQuery(document).ready(function($){
        
         $('.slider-for').slick({
          autoplay       : true,
          slidesToShow   : 1,
          slidesToScroll : 1,
          arrows         : true,
          fade           : true,
          asNavFor       : '.slider-nav'
        });
        
        $('.slider-nav').slick({
          slidesToShow   : 5,
          slidesToScroll : 1,
          asNavFor       : '.slider-for',
          dots           : false,
          centerMode     : true,
          focusOnSelect  : true,
          arrows         : false,
          autoplay       : false,
          responsive     : [
            { 
              breakpoint: 1200, 
              settings: { slidesToShow   : 3 }
            },
            { 
              breakpoint: 992, 
              settings: { slidesToShow   : 3 }
            },
            { 
              breakpoint: 768, 
              settings: { slidesToShow   : 3 }
            },
            { 
              breakpoint: 480, 
              settings: { slidesToShow   : 2 }
            },
          ]
        });


    });    

</script>