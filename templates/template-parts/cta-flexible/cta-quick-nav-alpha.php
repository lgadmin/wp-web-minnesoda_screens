<section class="<?php the_sub_field('background_colour'); ?> clearfix pt-sm pb-md">
	<div class="cta-quick-nav-alpha <?php the_sub_field('container'); ?>">
	
		<?php if (get_sub_field('cta_title')) : ?>
			<header class="page-title"><span class="h3"><?php the_sub_field('cta_title_content'); ?></span></header>
		<?php endif; ?>

		<div class="cta-body pt-sm">
			<?php if( have_rows('quick_nav_alpha') ): ?>
			   
			   <section class="quick-nav">
				<?php while( have_rows('quick_nav_alpha') ): the_row(); ?>
				    
				    <div class="quick-nav-item">
					
						<div class="thumbnail-quick">
							<div class="img-cont clearfix">
								<?php the_sub_field('quick_nav_image_icon'); ?>
							</div>
							<div class="caption">
								<?php the_sub_field('quick_nav_link_label_alpha'); ?>
							</div>
						</div>

				    </div>

				<?php endwhile; ?>
			   </section>

			<?php endif; ?>		
		</div>
		
	</div>
</section>



