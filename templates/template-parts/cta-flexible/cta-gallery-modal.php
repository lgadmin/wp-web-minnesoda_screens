
<section class="<?php the_sub_field('background_colour'); ?> clearfix pt-sm pb-md">
	<div class="cta-gallery-modal <?php the_sub_field('container'); ?>">
	
		<?php if (get_sub_field('cta_title')) : ?>
			<header class="page-title"><span class="h3"><?php the_sub_field('cta_title_content'); ?></span></header>
		<?php endif; ?>


		<div class="cta-body pt-sm">
			
			<?php if( have_rows('gallery_modal') ): ?>
			   
			   <section class="quick-nav">
				<?php while( have_rows('gallery_modal') ): the_row(); ?>
				    
				    <div class="quick-nav-item">

						<a href="#" class="thumbnail" target=".modal.gallery" gallery_id="<?php the_sub_field('gallery_modal_id'); ?>">
							<?php echo wp_get_attachment_image( get_sub_field('gallery_modal_image'), 'full-size' ); ?>
							<div class="caption">
								<?php the_sub_field('gallery_modal_label'); ?>
							</div>
						</a>

				    </div>

				<?php endwhile; ?>
			   </section>

			<?php endif; ?>
			
		</div>
	</div>
</section>

<!-- Modal -->
<div class="modal gallery fade" id="gallery-<?php echo $gallery_id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content"> 

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Gallery</h4>
      </div>

	      <div class="modal-body">	

	      </div>

    </div>
  </div>
</div>
