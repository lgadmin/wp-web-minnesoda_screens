<section class="<?php the_sub_field('background_colour'); ?> clearfix pt-sm pb-md">
	<div class="cta-youtube <?php the_sub_field('container'); ?>">
		<?php if (get_sub_field('cta_title')) : ?>
			<header class="page-title"><span class="h3"><?php the_sub_field('cta_title_content'); ?></span></header>
		<?php endif; ?>
		
		<div class="cta-body pt-sm">
			
			<?php if( have_rows('youtube_videos') ): ?>
			   
			   <section class="quick-nav">
				<?php while( have_rows('youtube_videos') ): the_row(); ?>
				    
				    <div class="quick-nav-item">
					
						<div class="thumbnail-quick" data-toggle="modal" data-target="#myYouTube">
							<div class="img-cont clearfix">
								<div class="embed-responsive embed-responsive-16by9">
									<?php the_sub_field('youtube_video'); ?>
								</div>
							</div>
						</div>

				    </div>

				<?php endwhile; ?>
			   </section>

			<?php endif; ?>	

		</div>
	</div>
</section>
