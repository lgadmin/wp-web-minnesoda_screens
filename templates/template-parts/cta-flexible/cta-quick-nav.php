<section class="<?php the_sub_field('background_colour'); ?> clearfix pt-sm pb-md">
	<div class="cta-quick-nav <?php the_sub_field('container'); ?>">
	
		<?php if (get_sub_field('cta_title')) : ?>
			<header class="page-title"><span class="h3"><?php the_sub_field('cta_title_content'); ?></span></header>
		<?php endif; ?>

		<div class="cta-body pt-sm">
			<?php if( have_rows('quick_nav') ): ?>
			   
			   <section class="quick-nav">
				<?php while( have_rows('quick_nav') ): the_row(); ?>
				    
				    <div class="quick-nav-item">
					
						<a href="<?php the_sub_field('quick_nav_link'); ?>" class="thumbnail-quick">
							<?php echo wp_get_attachment_image( get_sub_field('quick_nav_image'), 'full-size' ); ?>
							<div class="caption">
								<?php the_sub_field('quick_nav_link_label'); ?>
							</div>
						</a>

				    </div>

				<?php endwhile; ?>
			   </section>

			<?php endif; ?>		
		</div>
		
	</div>
</section>


