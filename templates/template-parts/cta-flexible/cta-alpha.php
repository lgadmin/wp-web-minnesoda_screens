<section class="<?php the_sub_field('background_colour'); ?> clearfix pt-md">
	<div class="cta-alpha <?php the_sub_field('container'); ?>">
		<?php if (get_sub_field('cta_title')) : ?>
			<header class="page-title"><span class="h3"><?php the_sub_field('cta_title_content'); ?></span></header>
		<?php endif; ?>
		
		<div class="cta-body pt-sm">
			<?php the_sub_field('alpha_cw'); ?>
		</div>
	</div>
</section>
