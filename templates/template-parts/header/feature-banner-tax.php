<?php $term = get_queried_object(); ?>

<?php if (get_field('banner_image', $term)) : ?>
	<?php 
		$buy_text = get_field('buy_button_text', $term);
		$buy_link = get_field('buy_button_link', $term);
	?>
	<div class="feature-banner">
		<div class="title-cont">
			<div class="banner-content">
				<span class="featured-title"><?php the_field('banner_copy', $term); ?></span>
				<div class="text-center feature-banner-buttons">
					<a href="<?php the_field('banner_button_link', $term); ?>" class="btn btn-primary mt-xs btn-lg"><?php the_field('banner_button_label', $term); ?></a>
					<?php if($buy_link && $buy_text) : ?>
						<a class="btn btn-primary" href="<?php echo $buy_link; ?>"><?php echo $buy_text; ?></a>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div class="img-cont fm-color-overlay">
			<?php echo wp_get_attachment_image( get_field('banner_image', $term), 'full-size' ); ?>
		</div>

	</div> 

<?php else : ?>

	<div class="feature-banner">

		<div class="title-cont">
			<div class="banner-content">
				<span class="featured-title"><?php the_field('default_banner_copy', 'option'); ?></span>
				<div class="text-center">
				<a href="<?php the_field('default_banner_button_link', 'option'); ?>" class="btn btn-primary mt-xs btn-lg"><?php the_field('default_banner_button_label', 'option'); ?></a>
			</div>
			</div>
		</div>

		<div class="img-cont fm-color-overlay">
			<?php echo wp_get_attachment_image( get_field('default_banner_image', 'option'), 'full-size' ); ?>
		</div>

	</div>
<?php endif; ?>