<?php $term = get_queried_object(); ?> <!-- screen doors product page is som -->
<?php if( have_rows('slider') ): ?>
   
    <section id="home-slider" class="feature-banner">

        <?php  while ( have_rows('slider') ) : the_row(); ?>
            <?php 
                $buy_text = get_sub_field('buy_button_text');
                $buy_link = get_sub_field('buy_button_link');
            ?>
            <div class="slide-container">
                <div class="slide-cta">
                    <?php the_sub_field('slider_cta_copy'); ?>
                    <?php if (get_sub_field('slider_button_label')) : ?>
                        <div class="text-center feature-banner-buttons">
                            <a href="<?php the_sub_field('slider_button_link'); ?>" class="btn btn-slider"><?php the_sub_field('slider_button_label'); ?></a>
                            <?php if($buy_link && $buy_text) : ?>
                            <a class="btn btn-primary" href="<?php echo $buy_link; ?>"><?php echo $buy_text; ?></a>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="title-cont">
                    <div class="banner-content">
                        <span class="featured-title"><?php the_field('default_banner_copy', 'option'); ?></span>
                        <div class="text-center">
                        <a href="<?php the_field('default_banner_button_link', 'option'); ?>" class="btn btn-primary mt-xs btn-lg"><?php the_field('default_banner_button_label', 'option'); ?></a>
                    </div>
                    </div>
                </div>

                <a href="#" class="scroll-down"><i class="fas fa-angle-down fa-4x"></i></a>
                
                <div class="fm-color-overlay">
    				<?php echo wp_get_attachment_image( get_sub_field('feature_slide'), 'full-size' ); ?>
                </div>
			</div>	
		<?php endwhile; ?>

    </section> 
<?php endif; ?>


<script>
    jQuery( document ).ready(function() {

        jQuery(window).on('load', function(){
            jQuery('#home-slider').find('.slick-slide[data-slick-index=0]').find('.slide-cta').addClass('animated');
        });

        jQuery('#home-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
          jQuery(this).find('.slick-slide .slide-cta').removeClass('animated');
          jQuery(this).find('.slick-slide[data-slick-index='+ nextSlide +']').find('.slide-cta').addClass('animated');
        });

        jQuery('#home-slider').slick({
            autoplay       : true,
            infinite       : true,
            slidesToShow   : 1,
            slidesToScroll : 1,
            arrows         : true,
            dots           : false,
            speed          : 1800,
            autoplayspeed  : 700,
        });

    });
</script>