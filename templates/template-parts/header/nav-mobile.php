<div class="header-main mobile">
  <div class="site-branding">
    <div class="logo">
      <?php
        echo do_shortcode('[lg-site-logo]');
      ?>
    </div>

    <div class="mobile-toggle"><i class="fa fa-bars" aria-hidden="true"></i></div>
  </div><!-- .site-branding -->

  <div class="main-navigation desktop">
      <nav class="navbar navbar-default">  
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-mobile-nav">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>


          <!-- Main Menu  -->
          <?php 

            $mainMenu = array(
              'menu'              => 'mobile-menu',
              // 'theme_location'    => 'top-nav',
              'depth'             => 2,
              'container'         => 'div',
              'container_class'   => 'collapse navbar-collapse',
              'container_id'      => 'main-mobile-nav',
              'menu_class'        => 'nav navbar-nav',
              'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                // 'walker'         => new WP_Bootstrap_Navwalker_Custom()  // Custom used in Skin Method.
              'walker'            => new WP_Bootstrap_Navwalker()
            );
            wp_nav_menu($mainMenu);

          ?>
      </nav>
  </div>

</div>
