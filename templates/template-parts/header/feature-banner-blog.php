<?php if (get_field('banner_image', 291)) : ?>
	<div class="feature-banner">

		<div class="title-cont">
			<div class="banner-content">
				<span class="featured-title"><?php the_field('banner_copy', 291); ?></span>
				<div class="text-center">
					<a href="<?php the_field('banner_button_link', 291); ?>" class="btn btn-primary mt-xs btn-lg"><?php the_field('banner_button_label', 291); ?></a>
				</div>
			</div>
		</div>

		<div class="img-cont fm-color-overlay">
			<?php echo wp_get_attachment_image( get_field('banner_image', 291), 'full-size' ); ?>
		</div>

	    <a href="#" class="scroll-down"><i class="fas fa-angle-down fa-4x"></i></a>

	</div> 

<?php else : ?>

	<div class="feature-banner">

		<div class="title-cont">
			<div class="banner-content">
				<span class="featured-title"><?php the_field('default_banner_copy', 'option'); ?></span>
				<div class="text-center">
				<a href="<?php the_field('default_banner_button_link', 'option'); ?>" class="btn btn-primary mt-xs btn-lg"><?php the_field('default_banner_button_label', 'option'); ?></a>
			</div>
			</div>
		</div>

		<div class="img-cont fm-color-overlay">
			<?php echo wp_get_attachment_image( get_field('default_banner_image', 'option'), 'full-size' ); ?>
		</div>

        <a href="#" class="scroll-down"><i class="fas fa-angle-down fa-4x"></i></a>


	</div>
<?php endif; ?>

