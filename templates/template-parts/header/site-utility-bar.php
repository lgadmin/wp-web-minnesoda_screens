<div class="utility-bar">
  <div class="container">
    
    <div class="company-name">
      <a href="<?php echo site_url(); ?>"><?php echo get_bloginfo( 'name' ); ?></a>
    </div>
    
    <div class="right">
    
      <div class="sub-phone">
        <a href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><span><?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></span></a>
      </div>
    
      <div class="sub-email">
        <a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><span><?php echo do_shortcode('[lg-email]'); ?></span></a>
      </div>

    </div>

  </div>
</div>