<?php if ( have_posts() ) : ?>
<?php $odd_or_even = 'odd'; ?>
	
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="mb-lg p-sm clearfix <?php echo $odd_or_even; ?>" >
			<?php $odd_or_even = ('odd'==$odd_or_even) ? 'even' : 'odd'; ?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class('type'); ?>>
								
				<div class="entry-content">
					<div class="post-copy">
						<?php the_content(); ?>
					</div>
				</div>

			</article>
		</div>
	<?php endwhile; ?>

<?php endif ?>