<?php
	$quote_cta_title = get_field('quote_cta_title', 'option');
	$quote_cta_content = get_field('quote_cta_content', 'option');
	$quote_cta_button_text = get_field('quote_cta_button_text', 'option');
	$quote_cta_button_link = get_field('quote_cta_button_link', 'option');
?>

<div class="pt-lg pb-lg center">
	<div class="container">
		<h2 class="h2 border-heading">
			<?php echo $quote_cta_title; ?>
		</h2>
		<?php echo $quote_cta_content; ?>
		<a href="<?php echo $quote_cta_button_link; ?>" class="cta-primary"><?php echo $quote_cta_button_text; ?></a>
	</div>
</div>