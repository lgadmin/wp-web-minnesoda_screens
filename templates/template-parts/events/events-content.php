<?php 
	$events_query = array(
        'post_type'     => 'events'
    );

    $events_query_results = new WP_Query( $events_query );
?>





<?php if ( $events_query_results->have_posts() ) : ?>
		
<h2>UPCOMING EVENTS</h2>
		<table class="events-table table-responsive">
			
			<thead>
			  <tr class="bg-primary">
			     <th class="event-date">Date</th>
			     <th class="event-details">Event Details</th>
			     <th class="event-location">Location</th>
			  </tr>
			 </thead>

			 <tbody>
				<?php while ( $events_query_results->have_posts() ) :  $events_query_results->the_post(); ?>
					
					<?php 
						$eventDate = get_field('event_date');
						$date_now = date("Y-m-d");
							
						if ( strtotime($date_now) < strtotime($eventDate)): 
					?>
						
					<tr>
						<td><p><?php the_field('event_date'); ?></p></td>
						<td><p><strong><?php echo get_the_title(); ?></strong></p></td>
						<td><p><?php the_field('event_location'); ?></p></td>
					</tr>

					<?php endif; ?>
				<?php endwhile; ?>
			</tbody>
		</table>
<?php endif ?>

<?php wp_reset_query(); ?>





<?php if ( $events_query_results->have_posts() ) : ?>
		
<h2>PAST EVENTS</h2>
	<table class="events-table table-responsive">
		
		<thead>
		  <tr class="bg-primary">
		     <th class="event-date">Date</th>
		     <th class="event-details">Event Details</th>
		     <th class="event-location">Location</th>
		  </tr>
		 </thead>

		 <tbody>
			<?php while ( $events_query_results->have_posts() ) :  $events_query_results->the_post(); ?>
				<?php 
					$eventDate = get_field('event_date');
					$date_now = date("Y-m-d");
						
					if ( strtotime($date_now) > strtotime($eventDate)): 
				?>

				<tr>
					<td><p><?php the_field('event_date'); ?></p></td>
					<td><p><strong><?php echo get_the_title(); ?></strong></p></td>
					<td><p><?php the_field('event_location'); ?></p></td>
				</tr>

			<?php endif; ?>

			<?php endwhile; ?>
		</tbody>
	</table>
<?php endif ?>

<?php wp_reset_query(); ?>