<?php if ( have_posts() ) : ?>
<?php $odd_or_even = 'odd'; ?>
	
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="mb-lg p-sm clearfix <?php echo $odd_or_even; ?>" >
			<?php $odd_or_even = ('odd'==$odd_or_even) ? 'even' : 'odd'; ?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class('type'); ?>>
				
				<header>
					<h2 class="post-title mb-0"><?php the_title(); ?></h2>
					<p class="small post-utillity"><?php echo the_date(); ?></p>
				</header>
				
				<div class="entry-content">
					<div class="image-cont">
						<?php if (has_post_thumbnail()) : ?> 
							<?php echo get_the_post_thumbnail() ?>
						<?php else : ?>
							<img src="<?php echo catch_that_image(); // See custom functions ?>" alt="">
						<?php endif; ?>
					</div>
					<div class="post-copy">
						<?php the_excerpt(); ?>
						<div class="text-center">
							<a href="<?php echo get_permalink(); ?>" class="btn btn-primary"> Read More...</a>
						</div>
					</div>
				</div>

			</article>
		</div>
	<?php endwhile; ?>

<?php endif ?>